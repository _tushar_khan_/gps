<?php
	/**
	 * Created by PhpStorm.
	 * User : Tushar Khan
	 * Year : 2019
	 * Date : 2/14/2019
	 * Time : 9:04 PM
	 * File : Helper.php
	 */
	
	class Helper{
		public function __construct(  ) {
		
		}
		
		/**
		 * @param $url
		 *
		 * @return null|string
		 */
		public function getDate( $string ) {
			$dateStr = str_split($string);
			$date = null;
			
			for ($i = 0; $i < 10; $i++) $date .= $dateStr[$i];
			
			return $date ;
		}
		
		
		public function getTime( $string ) {
			$dateStr = str_split($string);
			$len = sizeof( $dateStr );
			$date = null;
			
			for ($i = 14; $i < $len; $i++) $date .= $dateStr[$i];
			
			if ( ( (int) ($dateStr[11] . $dateStr[12]) ) >= 12  ) {
				if( ( (int) ($dateStr[11] . $dateStr[12]) ) > 12 ){
					$realTime = ( (int) ($dateStr[11] . $dateStr[12]) ) - 12;
				}
				return $realTime . ":" . $date . " PM";
			}
			
			if ( ( (int) ($dateStr[11] . $dateStr[12]) ) >= 00  ) return ($dateStr[11] . $dateStr[12]) .":" . $date . " AM";
		}
	}//main Class