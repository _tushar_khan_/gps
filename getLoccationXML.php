<?php

function allDevices()
{
    $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = ltrim( strstr( ltrim( strstr(strstr($url, '?'), '&'), '&' ), '=' ), '=' );
    $chars = $devices = array();
    $i = 0;
    $y = $url;

    foreach (str_split($url) as $key => $value)
        if ($value == '&')
            $i++;

    $j = $i;

    if ($i == 0) {
        $devices[0] = $url;
    } else {
        while ($j--) {
            $x = strstr($y, '&', true);
            array_push($devices, $x);
            $y = ltrim(strstr($y, '&'), '&');
            if ($j == 0) {
                array_push($devices, $y);
            }
        }
    }
    return $devices ;
}


header("Content-type: text/xml");
header("Access-Control-Allow-Origin: *");
    $deviceId = $_GET['userId'];
    $alldev = allDevices();
    require'lib/User.php';
    require 'lib/Helper.php';
    $user = new User();
    $helper = new Helper();
//
    $data = $user->getLocationByCheckbox( $alldev );
//
    $doc = new DomDocument('1.0');
    $node = $doc->createElement("markers");
    $parnode = $doc->appendChild($node);
    $i = 0;
    $length = sizeof($alldev);
//
    foreach ($data as $key => $value) {
       // print_r( $data );
        $node = $doc->createElement("marker");
        // echo "<br>" . $alldev[$i];
        $newnode = $parnode->appendChild($node);
        foreach ($value as $index => $val) {
			$newnode->setAttribute("mainId", $i);
			if ( $index == "date" ) {
				$newnode->setAttribute( "date", $helper->getDate( $val ) );
				$newnode->setAttribute( "time", $helper->getTime( $val ) );
			} else {
				$newnode->setAttribute( $index, $val );
				$newnode->setAttribute( "userId", $deviceId );
			}
        }

        if ($i == $length - 1) break;
        $i++;
    }
//
    echo $doc->saveXML();
// ?>
