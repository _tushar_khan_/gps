        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center text-capitalize text-info">
                        <?php date_default_timezone_set('Asia/Dhaka');  ?>
                        <p>&copy;  <?php echo date('l jS \of F Y h:i:s A'); ?></p>
                    </div>
                </div>
            </div>
        </footer>
        <?php
            //$result = $user->getLocatCurrentLion();
        ?>


        <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
        <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
                document.getElementById("main").style.marginLeft = "250px";
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
                document.getElementById("main").style.marginLeft= "0";
            }

            function openNav2() {
                document.getElementById("mySidenav2").style.width = "250px";
                document.getElementById("main2").style.marginLeft = "250px";
            }

            function closeNav2() {
                document.getElementById("mySidenav2").style.width = "0";
                document.getElementById("main2").style.marginLeft= "0";
            }


            function mainURL() {
                let devices = [];
                let url = '&devices=';
                let len = <?php echo $k; ?>;
                for (var i = 0; i < len - 1; i++) {
                    if (document.getElementById('myCheck' + i).checked) {
                        document.getElementById('dev' + (i + 1)).style.display = "block";
                        // document.getElementById('dev' + (i + 2)).style.display = "none";
                        devices[i] = document.getElementById('myCheck' + i).value;
                        ( i === 0 ) ? url += devices[i] + '&' : url += devices[i] + '&' ;
                    } else document.getElementById('dev' + (i + 1)).style.display = "none";
                }
                return url.slice(0, -1);
            }

        </script>


        <?php ( isset($id) && isset($k) ) ? $url = "js/map.js?userId={$id}&total={$k}" : $url = "js/map.js?userId=1&total=2" ?>
        <script type="text/javascript" src="<?php echo $url?>" id="myScript"></script>
<script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGzvc3nleYYlI6kmHUQF-xQw9pv1luztw&callback=mapInit&language=bn&region=BG">
        </script>
    </body>
</html>
