<?php
/**
 * Created by PhpStorm.
 * User: Tanzil
 * Date: 4/25/2017
 * Time: 2:33 PM
 */

    $filepath = realpath(dirname(__FILE__));
    include_once $filepath . '\lib\Session.php';
    Session::init();
    Session::checkSession();
    $user = new User();
    $helper = new Helper();
    $id = Session::get('id');
    $userData = $user->getLocation($id);
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
        <title>Home</title>

        <style>
body {
  font-family: "Lato", sans-serif;
  font-size: 16px !important;
  background : rgb( 225, 218, 202 );
}

.panel{
    margin-bottom : 0px !important;
}

#date p, #time p, #la p, #sat p, #spd p{
    padding: 22px 0 0 0;
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: -2px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>

    </head>
    <?php
        if(isset($_GET['action']) && $_GET['action'] == "logout")
        {
            Session::sessionDestroy();
//            Session::sessionDestroy();
            echo "<script>window.location='login.php'</script>";

        }//Logging out
    ?>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Home</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                            $userId = Session::get("id");
                            $userLogin = Session::get("login");
                            if($userLogin == true):
                        ?>
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="profile.php?id=<?php echo $userId; ?>">Profile</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">All Devices <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <?php   foreach($userData as $key => $value):  ?>
                                      <li><a href="#"><?php echo $value['device_id'];  ?></a></li>
                                      <?php endforeach; ?>
<!--                                    --><?php //print_r( $userData ); ?>
                                </ul>
                              </li>
                        <?php else: ?>
                            <li>
                                <a href="login.php">Login</a>
                            </li>
                            <li>
                                <a href="register.php">Register</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
