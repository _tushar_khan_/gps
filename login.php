<?php
/**
 * Created by PhpStorm.
 * User: Tanzil
 * Date: 4/25/2017
 * Time: 4:58 PM
 */
 
 //require 'head.php';
	$filepath = realpath(dirname(__FILE__));
	include_once 'lib/Session.php';
	Session::init();
    include 'lib/User.php';
    $user = new User();
    Session::checkLogin();
 
 
?>
<?php
    if(($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['login']) )
    {
        $user->userLogin($_POST);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
        <title>Login</title>
    </heade>    

<body>
    <div class="container">
        <div class="row">
            <div class="panel-default panel">
                <div class="page-header">
                    <h3 class="text-capitalize text-center">  user login  </h3>
                </div>

                <div class="panel-body">
                    <div class="col-md-7 col-md-push-2" >
                        <form action="" method="POST">
                        <!--
                            Checking User Registration
                            if true Return $userRegit
                        -->

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-primary" name="login">Login</button>
                    </form>
                    </div>
                </div>
            </div>
        </div><!--row-->
    </div><!--container-->
<?php  require 'footer.php' ;?>
