<?php include 'lib/User.php'; ?>
<?php include 'lib/Helper.php'; ?>
<?php include 'head.php' ?>

    <div class="container-fluid" >
        <div class="row">

            <div class="col-md-12 " id="main">
                <h5 class="" style="position: relative; left: 10px">
                    <span style="font-size:15px;cursor:pointer" onclick="openNav()">&#9776; Usr</span>
                    <span style="font-size:15px;cursor:pointer" onclick="openNav2()">&#9776;Car Information</span>
                </h5>
            </div>

            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <div class="col-md-11 " style="margin:0 auto">
                    <img src="<?php echo Session::get("image") ?>" class="img-responsive" width="100%" />
                  <h4 class="text-center text-success">Username : <?php echo Session::get("name") ?></h4>
                  <h4 class="text-center text-success">E-mail : <?php echo Session::get("email") ?></h4>
                    <a href="?action=logout" class="btn btn-info">Logout</a>
                </div>
            </div>

            <div class="col-md-12">
                <?php if( $userData ): ?>
                    <form>
                        <?php $i = 0; foreach ($userData as $key => $value): ?>
                            <label class="checkbox-inline">
                              <input type="checkbox" id="myCheck<?php echo $i; ?>" <?php if( ! $i ): ?> checked <?php endif; ?> value="<?php echo $value['device_id']; ?>"><?php echo $value['device_id']; ?>
                            </label>
                        <?php $i++; endforeach; ?>
                    </form>
                <?php endif; ?>
            </div>

            <div class="col-md-12">
                <div id="mySidenav2" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav2()">&times;</a>
                    <div class="col-md-11 text-center" style="margin:0 auto" id="infos">
                        <div class="">
                            <?php $k = 1; foreach ($userData as $key => $value): ?>
                                <div class="" style="display: none; color : #FFFFFF "  id="dev<?php echo $k?>">
                                    <p class=" btn btn-info" style="margin: 10px ;"><?php echo $value['device_id']; ?></p>
                                    <div class="col-md-12 " >
                                         <p id="date<?php echo $k?>">Date : <?php echo $helper->getDate($value['date'])?></p>
                                     </div>

                                     <div class="col-md-12" >
                                         <p id="time<?php echo $k?>">Time : <?php echo $helper->getTime($value['date'])?></p>
                                     </div>

                                     <div class="col-md-12" >
                                         <p id="la<?php echo $k?>">Latitude : <?php echo $value['la']?></p>
                                     </div>

                                     <div class="col-md-12" >
                                         <p id="ln<?php echo $k?>">Longitude : <?php echo $value['ln']?></p>
                                     </div>

                                     <div class="col-md-12" >
                                         <p id="sat<?php echo $k?>">Satellite : <?php echo $value['sat']?></p>
                                     </div>

                                     <div class="col-md-12" >
                                         <p id="spd<?php echo $k?>">Speed : <?php echo $value['spd']?></p>
                                     </div>
                                </div>
                            <?php $k++; endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
                    <div id="map" style="width: 100%; height: 560px"></div>
        </div><!--row-->
    </div><!--container-->
    <!--<div id="map" style="width: 100%; height: 0px"></div>-->
<?php  require 'footer.php' ;?>
